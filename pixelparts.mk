
# Camera
PRODUCT_BROKEN_VERIFY_USES_LIBRARIES := true
PRODUCT_PACKAGES += \
    GoogleCamera

ifeq ($(PRODUCT_SUPPORTS_CLEAR_CALLING), true)
PRODUCT_PACKAGES += \
    ClearCallingSettingsOverlay
endif

# Dolby
BOARD_VENDOR_SEPOLICY_DIRS += \
    vendor/google/pixelparts/sepolicy/dolby

# Health
TARGET_HEALTH_CHARGING_CONTROL_SUPPORTS_BYPASS := false
TARGET_HEALTH_CHARGING_CONTROL_SUPPORTS_DEADLINE := true
TARGET_HEALTH_CHARGING_CONTROL_SUPPORTS_TOGGLE := false

PRODUCT_PACKAGES += \
    vendor.lineage.health-service.default

BOARD_VENDOR_SEPOLICY_DIRS += \
    vendor/google/pixelparts/sepolicy/health

# Parts
PRODUCT_PACKAGES += \
    GoogleParts \
    PixelFrameworksOverlay \
